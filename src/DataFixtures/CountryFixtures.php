<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Country;

class CountryFixtures extends Fixture
{
    public $countries = [
        ['name' => "Германия", 'taxNumberFormat' => '/^DE\d{9}$/', 'code' => 'DE', 'nalogValue' => 19],
        ['name' => "Италия", 'taxNumberFormat' => '/^IT\d{11}$/', 'code' => 'IT', 'nalogValue' => 22],
        ['name' => "Греция", 'taxNumberFormat' => '/^GR\d{9}$/', 'code' => 'GR', 'nalogValue' => 24],
        ['name' => "Франция", 'taxNumberFormat' => '/^FR[A-Z]{2}\d{9}$/', 'code' => 'FR', 'nalogValue' => 20],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->countries as $countryData) {
            $country = new Country();
            foreach ($countryData as $key => $value) {
                $methodName = 'set' . ucfirst($key);
                $country->$methodName($value);
            }
            $manager->persist($country);
        }

        $manager->flush();
    }
}

