<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Product;

class ProductFixtures extends Fixture
{
    public $products = [
        ['name' => "Iphone"],
        ['name' => "Наушники"],
        ['name' => "Чехол"],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->products as $productData) {
            $product = new Product();
            $product->setName($productData['name']);
            $product->setPrice(mt_rand(10, 100));
            $manager->persist($product);
        }

        $manager->flush();
    }
}
