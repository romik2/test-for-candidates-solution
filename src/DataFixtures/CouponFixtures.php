<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\InterestCoupon;
use App\Entity\CashCoupon;

class CouponFixtures extends Fixture
{
    public $coupons = [
        ['class' => CashCoupon::class, 'code' => 'D15', 'valueDiscounts' => '15'],
        ['class' => InterestCoupon::class, 'code' => 'P100', 'valueDiscounts' => '100'],
        ['class' => InterestCoupon::class, 'code' => 'P10', 'valueDiscounts' => '10'],
        ['class' => InterestCoupon::class, 'code' => 'P20', 'valueDiscounts' => '20'],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->coupons as $couponData) {
            $coupon = new $couponData['class']();
            array_shift($couponData); 
            foreach ($couponData as $key => $value) {
                $methodName = 'set' . ucfirst($key);
                $coupon->$methodName($value);
            }
            $manager->persist($coupon);
        }

        $manager->flush();
    }
}
