<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ProductService;
use App\PaymentProcessor\PaypalPaymentProcessor;
use App\PaymentProcessor\StripePaymentProcessor;
use Symfony\Component\HttpFoundation\Response;

class PaymentController extends AbstractController
{
    /**
     * @Route("/calculate-price", name="calculate_price", methods={"POST"})
     */
    public function calculatePrice(Request $request, ProductService $productService): JsonResponse
    {
        try {
            $params = json_decode($request->getContent(), true);
            $price = $productService->calculatePrice($params['product'], $params['couponCode'], $params['taxNumber']);
        } catch (\Exception $ex) {
            return $this->json([
                'result' => false,
                'messages' => $ex->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->json([
            'result' => true,
            'price' => $price,
        ]);
    }

    /**
     * @Route("/purchase", name="purchase", methods={"POST"})
     */
    public function purchase(Request $request, ProductService $productService): JsonResponse
    {
        try {
            $params = json_decode($request->getContent(), true);
            $price = $productService->calculatePrice($params['product'], $params['couponCode'], $params['taxNumber']);
            $class = 'App\PaymentProcessor\\' . ucfirst($params['paymentProcessor']) . 'PaymentProcessor';
            $payment = new $class();

            return $this->json([
                'result' => $payment->pay($price),
            ]);
        } catch (\Exception $ex) {
            return $this->json([
                'result' => false,
                'messages' => $ex->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
