<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Country;
use App\Entity\Product;
use App\Entity\CashCoupon;
use App\Entity\InterestCoupon;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Test For Candidates Solution');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Страны', 'fas fa-list', Country::class);
        yield MenuItem::linkToCrud('Продукты', 'fas fa-list', Product::class);
        yield MenuItem::linkToCrud('Денежные купоны', 'fas fa-list', CashCoupon::class);
        yield MenuItem::linkToCrud('Процентные купоны', 'fas fa-list', InterestCoupon::class);
    }
}
