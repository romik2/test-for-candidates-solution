<?php

namespace App\Controller\Admin;

use App\Entity\InterestCoupon;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class InterestCouponCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return InterestCoupon::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
