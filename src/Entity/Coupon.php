<?php

namespace App\Entity;

use App\Repository\CouponRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CouponRepository::class")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "CashCoupon": "App\Entity\CashCoupon",
 *     "InterestCoupon": "App\Entity\InterestCoupon",
 * })
 */
abstract class Coupon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $valueDiscounts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getValueDiscounts(): ?string
    {
        return $this->valueDiscounts;
    }

    public function setValueDiscounts(string $valueDiscounts): self
    {
        $this->valueDiscounts = $valueDiscounts;

        return $this;
    }
}
