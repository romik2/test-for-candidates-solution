<?php

namespace App\Entity;

use App\Repository\CashCouponRepository;
use App\Entity\Coupon;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CashCouponRepository::class)
 */
class CashCoupon extends Coupon
{
    public static function getType(): string
    {
        return static::class;
    }

    public function calculateDiscount(float $price): float
    {
        return $price - $this->getValueDiscounts();
    }
}
