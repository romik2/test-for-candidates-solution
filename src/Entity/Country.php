<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $taxNumberFormat;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $nalogValue;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTaxNumberFormat(): ?string
    {
        return $this->taxNumberFormat;
    }

    public function setTaxNumberFormat(string $taxNumberFormat): self
    {
        $this->taxNumberFormat = $taxNumberFormat;

        return $this;
    }

    public function getNalogValue(): ?string
    {
        return $this->nalogValue;
    }

    public function setNalogValue(string $nalogValue): self
    {
        $this->nalogValue = $nalogValue;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
