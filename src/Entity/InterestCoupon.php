<?php

namespace App\Entity;

use App\Repository\InterestCouponRepository;
use App\Entity\Coupon;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InterestCouponRepository::class)
 */
class InterestCoupon extends Coupon
{
    public static function getType(): string
    {
        return static::class;
    }

    public function calculateDiscount(float $price): float
    {
        return $price * (1 - ($this->getValueDiscounts() / 100));
    }
}
