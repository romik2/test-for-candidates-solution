<?php

namespace App\Service;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\CouponRepository;
use App\Repository\ProductRepository;
use App\Repository\CountryRepository;
use Exception;

class ProductService
{
    private $couponRepository;
    private $productRepository;
    private $countryRepository;

    public function __construct(CouponRepository $couponRepository, ProductRepository $productRepository, CountryRepository $countryRepository)
    {
        $this->couponRepository = $couponRepository;
        $this->productRepository = $productRepository;
        $this->countryRepository = $countryRepository;
    }

    public function calculatePrice($product, $couponCode, $taxNumber)
    {
        $validator = Validation::createValidator();

        $product = $this->productRepository->find($product);
        if (empty($product)) {
            throw new Exception('Product not found!');
        }

        $coupon = $this->couponRepository->findOneBy(['code' => !empty($couponCode) ? $couponCode : null]);

        $codeCountry = substr($taxNumber, 0, 2);
        $country = $this->countryRepository->findOneBy(['code' => $codeCountry]);

        $constraint = new Assert\Regex([
            'pattern' => $country->getTaxNumberFormat(),
            'message' => 'Invalid tax number format'
        ]);

        $errors = $validator->validate($taxNumber, $constraint);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                throw new Exception($error->getMessage());
            }
        }

        $price = $product->getPrice() * (1 + ($country->getNalogValue() / 100));
        if (!empty($coupon)) {
            return round($coupon->calculateDiscount($price), 2);
        }

        return round($price, 2);
    }
}