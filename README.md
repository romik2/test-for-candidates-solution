# test-for-candidates-solution

## Запуск проекта

Для старта проекта склонируйте репозиторий, после внутри выполните команду:
```
docker-composer up --build -d
```

Запуск сервера происходит в течении 2-5 минут, после build.

## Запросы

1. Получить цену для рассчёта
```
curl -X POST http://127.0.0.1:8003/calculate-price \
-H "Content-Type: application/json" \
-d '
{
    "product": 1,
    "taxNumber": "DE123456789",
    "couponCode": "D15"
}'
```
Ответ:
```
{"result":true,"price":29.03}
```

2. Проведение покупки
```
curl -X POST http://127.0.0.1:8003/purchase \
-H "Content-Type: application/json" \
-d '
{
    "product": 1,
    "taxNumber": "DE123456789",
    "couponCode": "D15",
    "paymentProcessor": "paypal"
}'
```
Ответ:
```
{"result":true}
```

3. Проведение покупки обработка с ошибкой
```
curl -X POST http://127.0.0.1:8003/purchase \
-H "Content-Type: application/json" \
-d '
{
    "product": 1,
    "taxNumber": "DE1234567891",
    "couponCode": "D15",
    "paymentProcessor": "paypal"
}'
```
Ответ:
```
{"result":false,"messages":"Invalid tax number format"}
```

4. Получить цену для рассчёта обработка с ошибкой
```
curl -X POST http://127.0.0.1:8003/calculate-price \
-H "Content-Type: application/json" \
-d '
{
    "product": 1,
    "taxNumber": "DE1234567891",
    "couponCode": "D15",
    "paymentProcessor": "paypal"
}'
```
Ответ:
```
{"result":false,"messages":"Invalid tax number format"}
```

Примечание: В проекте есть панель администратора, для изменения не значительных данных для теста и просмотра данных в базе. Она доступна по адресу (http://127.0.0.1:8003)