FROM ubuntu:20.04

RUN apt-get update
RUN apt-get install software-properties-common -y
RUN add-apt-repository ppa:ondrej/php
RUN apt-get update
RUN apt-get install php7.4 php7.4-fpm php7.4-common php7.4-mysql php7.4-xml php7.4-xmlrpc php7.4-curl php7.4-gd php7.4-imagick php7.4-cli php7.4-dev php7.4-imap php7.4-mbstring php7.4-opcache php7.4-soap php7.4-zip php7.4-intl php7.4-bcmath unzip -y
COPY . /app
RUN cd /app && php composer.phar install
CMD bash -c "cd /app && echo 'The server will start working in 5 minutes' && sleep 120 && php bin/console doctrine:migrations:migrate --no-interaction && php bin/console doctrine:fixtures:load --append --no-interaction && php -S 0.0.0.0:8003 -t public"